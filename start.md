# 前端启动命令

### 守护进程启动

```shell
nohup npm run dev > server.log 2>server.log &

```

### 查看是否在运行

```shell
lsof -i:8081
```

### jenkins自动化部署的重启脚本

#### 前端停止

```shell
pid = `lsof -i:8081 |grep *:tproxy | awk '{print $2}'`
if [-n "$pid"]
then 
  kill -9 $pid
fi 
```

#### 前端启动

```shell
nohup npm run dev > server.log 2>server.log &
```


#### 后端停止
```shell
pid = `ps -ef | grep java | grep  my-app.jar | awk '{print $2}'`
if [-n "$pid"]
then 
  kill -9 $pid
fi 
```

#### 后端启动

```shell
chmod 777 /usr/local/gamebook/soft/my-app.jar
cd /usr/local/gamebook/soft/
nohup ${JAVA_HOME}/bin/java -jar my-app.jar > server.log 2>server.log &
```